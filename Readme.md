# Default proejct setup

This is the beginning of standardising my project build flow.

Please branch from develop into your own feature branch, then merge it back in.

# To use
* `grunt build`
* `grunt watch`
* `grunt bump:minor`

# Todo
* https://github.com/gruntjs/grunt-contrib-bump
* Selenium tests
* Phonegap ready
* lock down merging to master

# To get running
1. NodeJs
    * [http://nodejs.org/]
1. Homebrew
    * [http://brew.sh/]
1. Install Phantom JS
    * [http://phantomjs.org/download.html]
1. Install Phonegap
    `sudo npm install -g phonegap`
1. Install Ant
    * `brew install ant`
1. Sass installed
    * `gem install sass`
1. Run `npm install`

var Charity = function (__id, __name, __description, __bitcoinAddress, __furtherLink)
{
	var self = this;
	var Charity = self;

	self.id            = ko.observable(null);
	self.name          = ko.observable(null);
	self.description   = ko.observable(null);
	self.bitcoinAddress = ko.observable(null);
	self.furtherLink   = ko.observable(null);
	self.lat           = ko.observable(null);
	self.long          = ko.observable(null);
	self.isGlobal       = ko.observable(null);

	// ahh that's nice
	return Charity;
};
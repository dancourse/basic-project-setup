var UserViewModel = function (__config)
{
	var self = this;

    var _charities = ko.observableArray(null);

    self.randomCharity = ko.observable(null);

    var _init = function ()
    {
        for(var x in __config)
        {
            var tmp_found = __config[x];
            var tmp_charity = new Charity();
            tmp_charity.id(tmp_found.id);
            tmp_charity.name(tmp_found.name);
            tmp_charity.description(tmp_found.description);
            tmp_charity.furtherLink(tmp_found.furtherLink);
            tmp_charity.bitcoinAddress(tmp_found.bitcoinAddress);

            _charities.push(tmp_charity);
        }

        var rndval = Math.floor(Math.random()*_charities().length);

        self.randomCharity(_charities()[rndval]);

        console.log('randomCharity', self.randomCharity().name());
    };

    self.buisnessNameFull = ko.observable("Bitcoin to Charity");
    self.minGBPToPersonallyContactCharity = ko.observable(100);
    self.minPercGoesToCharity = ko.observable(98);

    _init();
};
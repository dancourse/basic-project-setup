/* jshint -W099 */

module.exports = function(grunt) {

    var urls        = {};
    urls.platforms  = {};
    urls.build      = {};

    // platforms
        urls.platforms.www = "platforms/www/";

    // build
        urls.build.html         = "build/html/";
        urls.build.js           = "build/js/";
        urls.build.jsApp        = urls.build.js + 'app/';
        urls.build.jsLib        = urls.build.js + 'lib/';
        urls.build.sass         = "build/sass/";
        urls.build.imgSprites   = "build/img/sprites/";
        urls.build.imgOther     = "build/img/other/";

        urls.build.csv2json     = "build/data/";

    // other
        var _url_sass = {};
        _url_sass[ urls.build.sass + "/main.css" ] = urls.build.sass + 'base.scss';

    // _ports
        var _ports = {};
        _ports.localDev = 9001;

    // build sets
        var _spritesheet = ['sprite:one', 'copy:spriteImg', 'copy:spriteSCSS'];

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    convert: {
      options: {
        explicitArray: false,
      },
      charityData: {
        src: [urls.build.csv2json + '*.csv'],
        dest: urls.build.csv2json + 'Charities.json'
      }
    },
    connect:
    {
      build: {
        options: {
          port: _ports.localDev,
          base: urls.platforms.www + '',
          keepalive: true
        }
      }
    },
    sass: {                                     // Task
        dist: {                                 // Target
            files: _url_sass
        },
    },
    copy: {
      html: {
        src: urls.build.html + 'index.html',
        dest: urls.platforms.www + 'index.html',
        flatten: true,

      },
      sass: {
        src: urls.build.sass + 'base.scss',
        dest: urls.platforms.www + 'css/main.css',
        flatten: true,

      },
      imgOther: {
        src: urls.build.imgOther + '*',
        dest: urls.platforms.www + 'css/img/other/',
        flatten: true,
        expand: true,
      },
      imgSprites: {
        src: urls.build.imgSprites + 'gen/one.png',
        dest: urls.platforms.www + 'css/img/sprites/',
        flatten: true,
        expand: true,
      },
      spriteSCSS: {
        src: urls.build.imgSprites + 'gen/_sprite.scss',
        dest: urls.platforms.www + urls.build.sass + '_sprite.scss',
        flatten: true,
        expand: true,
      },
      jsExtLib: {
        src: urls.build.jsLib + '*',
        dest: urls.platforms.www + 'js/lib/',
        flatten: true,
        expand: true,
      },
      jsApp: {
        src: urls.build.jsApp + '*',
        dest: urls.platforms.www + 'js/app/',
        flatten: true,
        expand: true,
      },
      data: {
        src: urls.build.csv2json + '*.json',
        dest: urls.platforms.www + '/',
        flatten: true,
        expand: true,
      }
    },
    jshint: { // runs jshint on javascript files
      options: {
        smarttabs: false
      },
      all: {
        files: {
          src: ['Gruntfile.js', urls.build.jsApp + '/*.js']
        },
      }
    },
    sprite:{
        one: {
            src:        urls.build.imgSprites + '*.png',
            destImg:    urls.build.imgSprites + 'gen/one.png',
            destCSS:    urls.build.imgSprites + 'gen/_sprite.scss',

            imgPath:    '/images/sprites/one.png'
        },
    },

    watch: {
        sass: {
          files: [urls.build.sass + "*.scss"],
          tasks: ['sass', 'copy:sass'],
          options: {
            // Start a live reload server on the default port 35729
            livereload: _ports.localDev,
          },
        },
        data: {
          files: [urls.build.csv2json + "*.csv"],
          tasks: ['convert:charityData', 'copy:data'],
          options: {
            // Start a live reload server on the default port 35729
            livereload: _ports.localDev,
          },
        },
        jsApp: {
          files: [urls.build.jsApp + '*.js'],
          tasks: ['jshint', 'copy:jsApp'],
          options: {
            // Start a live reload server on the default port 35729
            livereload: _ports.localDev,
          },
        },
        jsLib: {
          files: [urls.build.jsLib + '*.js'],
          tasks: ['copy:jsLib'],
          options: {
            // Start a live reload server on the default port 35729
            livereload: _ports.localDev,
          },
        },
        html: {
          files: [urls.build.html + '*.html'],
          tasks: ['copy:html'],
          options: {
            // Start a live reload server on the default port 35729
            livereload: _ports.localDev,
          },
        },
        spritesImg: {
          files: [urls.build.sprites + '*.png'],
          tasks: _spritesheet,
          options: {
            // Start another live reload server on port 1337
            livereload: _ports.localDev,
          },
        },
      },
      karma: {
        unit: {
            configFile: 'tests/Karma/karma.conf.js'
        },
      },
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-express-server');
  grunt.loadNpmTasks('grunt-convert');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-spritesmith');
  grunt.loadNpmTasks('grunt-bump');

  // Default task(s).
  grunt.registerTask('build', ['convert', 'sass', 'jshint', 'copy']);

};